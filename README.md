# Trabalho Prático 4 - Controlador fussy
Evandro Chagas Ribeiro da Rosa - 15100732

Novembro 2017

## Introdução

O trabalho visa implementar o controlador de um caminhão que tem como objetivo estacionar
em uma doca. O problema esta abstraído em um aplicação java e temos acesso as seguintes
informações, posição x, y e angulo, em relação ao eixo x, do caminhão.

Essa três informações são usadas como entradas para o controlador, e obtemos como saída 
o angula que o caminhão tem que virar para conseguirmos estacionar na doca.

O controlador é implementado utilizando logica fussy, para isso utilizamos o a biblioteca
JFuzzyLogic para java.

## Implementação

A implementação foi feita em java e todo código está disponível em ```src/```,
as bibliotecas necessárias em ```lib/``` e o arquivo de configuração da logica fussy
em ```flc/```.

O arquivo arquivo ```remoteDriver.jar``` possui o código compilado e configurado para se
conectar em ```localhost:4321```. Ele precisa ser executado no mesmo diretório que se
encontra ```flc/``` e ```lib/```.

## Sistema fuzzy

JFuzzyLogic trabalha com arquivos escritos em *Fussy control language*, neles são
descritos: vareáveis de entrada de saída, funções de pertinência, método de
defuzzificação, entre outros parâmetros.

O Arquivo utilizado no trabalho esta disponível em ```fcl/driver.fcl```, a baixo segue uma 
pequena explicação sobre o mesmo.

### Vareáveis de entrada e saída

A baixo são declaradas as vareáveis de entrada *horizon*, *hight* e *front* do 
tipo ```REAL```, único tipo implementado pelo JFuzzyLogic.

```
// Define input variables
VAR_INPUT
    horizon : REAL; // x
    hight   : REAL; // y
    front   : REAL; // angle
END_VAR
```

Da mesma forma é definido a variável de saída *control*.

```
// Define output variable
VAR_OUTPUT
    control : REAL;
END_VAR
```

### Função de pertinência

As funções de pertinência para as vareáveis de entradas são declaradas como segue a baixo.

```
// Fuzzify input variable 'horizon'
FUZZIFY horizon
    TERM left   := gauss 0 0.14;
    TERM center := gauss 0.5 0.15;
    TERM right  := gauss 1 0.14;
END_FUZZIFY
```

Após o ```:=``` é especificado a função para o termo a esquerda, as funções podem ser:

* Triangular:
```
trian min mid max
```

* Trapezoidal:
```
trape min midLow midHigh max
```

* Gauss: 
```
gauss mean stdev
```

* Sino:
```
gbell a b mean
```

* Sigmoidal:
```
sigm gain center
```

* Singleton: apenas um número

* Linear por partes: 
```
(x_1, y_1) (x_2, y_2) .... (x_n, y_n)
```

Para às vareáveis de saída as funções de pertinência são definidas igualmente.

```
// Defzzzify output variable 'control'
DEFUZZIFY control
    TERM turn_left  := trian -2 -1 0;
    TERM turn_right := trian  0  1 2;
    TERM continue   := trian -0.001 0 0.001;
...
```

### Método de defuzzificação

O método é definido dentro do bloco ```DEFUZZIFY``` da variável de saída com:

```
...
    // Use 'Center Of Gravity' defuzzification method
    METHOD : COG;
...
```

Foi utilizado centro de gravidade para defuzzificação, único método que encontramos para
o JFuzzyLogic.

### Regras 

As regras são definidas dentro do bloco ```RULEBLOCK```, como o exemplo abaixo:

```
  RULE 2: if hight is top and (horizon is center or horizon is right) and front is west
          then control is turn_left;
```

É especificado o nome da regra seguido de ```: if``` parâmetros de entrada  ```then```
resultado da regra.

# Conjuntos fussy

![horizon](img/horizon.png) 

A variável x (*horizon*) poder pertencer a três conjuntos definido pelas seguintes funções
de pertinência:

* *left*: gauss com média 0 e desvio padrão 0,14
* *center*: gauss com média 0,5 e desvio padrão 0,15
* *richt*: gauss com média 1 e desvio padrão 0,14

![higth](img/higth.png)

A variável y (*higth*) pode pertencer ao conjunto *top* com a função de pertinência
sigmoidal com grau de inclinação -10 e média 0.5.

![front](img/front.png)

A variável angulo (*front*) pode pertencer aos seguintes conjuntos:

* *east* dividido em outros dois conjuntos
    - *eastb*: gauss com média 0 e desvio padrão 25
    - *easte*: gauss com média 360 e desvio padrão 25
* *north*: gauss com média 90 e desvio padrão 25
* *west*: gauss com média 180 e desvio padrão 25
* *south* dividido em outros dois conjuntos
    - *southb*: gauss com média 270 e desvio padrão 25
    - *southe*: gauss com média -90 e desvio padrão 25

*east* e *south* foram divididos em dois para conseguir expressar a característica 
circular da variável.

![control](img/control.png)

A variável de saída *control* pode pertencer aos seguintes conjuntos:

* *turn_left*: triangular com minimo -2, média -1 e máximo 0
* *turn_rigth*: triangular com minimo 0, média 1 e máximo 2
* *continue*: triangular com minimo -0,001, média 0 e máximo 0,001

*continue* é definida assim para que regra que utilize ela não tenha muita influencia em 
outras regras.

# Regras

Regras utilizadas no controlador:

* Regra 1: se (*center* ou *left*) e *east* então *turn_right*
* Regra 2: se (*center* ou *right*) e *west* então *turn_left*
* Regra 3: se *left* e *west* então *turn_left*
* Regra 4: se *right* e *east* então *turn_right*
* Regra 5: se *center* e *nouth* então *continue*
* Regra 6: se *left* e *south* então *turn_left*
* Regra 7: se *right* e *south* então *turn_right*
* Regra 8: se *left* e *nouth* então *turn_right*
* Regra 9: se *right* e *nouth* então *turn_left*

Todas as regras levam em consideração a varável *higth*.

# Conclusão

A lógica fussy se apresentou como uma maneira rápida e fácil de implementar o controlador,
com apenas 9 regras conseguimos eficientemente estacionar o caminhão, para valores de y
acima de 0,5.

A maior dificuldade foi definir as funções da pertinência para conseguir fazer um melhor 
proveito das regras, uma abordagem de tentativa e erro foi utilizada para configuração dos
conjuntos.
